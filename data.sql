CREATE SEQUENCE auditlogsequene;

 CREATE OR REPLACE FUNCTION AuditLogFuction()
 RETURNS "trigger" AS
 $BODY$
 BEGIN
   New.id:=nextval('auditlogsequene');
   Return NEW;
 END;
 $BODY$
 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER AuditLogIdTrigger
 BEFORE INSERT
 ON Audit_Log
 FOR EACH ROW
 EXECUTE PROCEDURE AuditLogFuction();
 
 CREATE SEQUENCE financialreportsequene;

 CREATE OR REPLACE FUNCTION financialreportFuction()
 RETURNS "trigger" AS
 $BODY$
 BEGIN
   New.id:=nextval('financialreportsequene');
   Return NEW;
 END;
 $BODY$
 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER financialreportIdTrigger
 BEFORE INSERT
 ON Financial_Report
 FOR EACH ROW
 EXECUTE PROCEDURE financialreportFuction();
 
  CREATE SEQUENCE userssequene;

 CREATE OR REPLACE FUNCTION usersFuction()
 RETURNS "trigger" AS
 $BODY$
 BEGIN
   New.id:=nextval('userssequene');
   Return NEW;
 END;
 $BODY$
 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER usersIdTrigger
 BEFORE INSERT
 ON users
 FOR EACH ROW
 EXECUTE PROCEDURE usersFuction();