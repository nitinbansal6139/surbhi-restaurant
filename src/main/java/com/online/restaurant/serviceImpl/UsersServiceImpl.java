package com.online.restaurant.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.online.restaurant.entity.Users;
import com.online.restaurant.repository.UsersRepository;
import com.online.restaurant.service.UsersService;

@Service
public class UsersServiceImpl implements UsersService{

	@Autowired
	UsersRepository usersRepository;

	@Override
	public String registerUser(String username, String password) {
		Users user = new Users();
		user.setUserName(username);
		user.setPassword(password);
		user.setRole("USER");
		usersRepository.save(user);
		return "User registered";
	}

	@Override
	public String registerUserbyAdmin(Users user) {
		usersRepository.save(user);
		return "User registered";
	}

	@Override
	public String updateUserPassword (String username, String password) throws Exception{
		Optional<Users> updateUser = usersRepository.findById(username);
		if(updateUser.isPresent()) {
			Users user = updateUser.get();
			user.setPassword(password);
			usersRepository.saveAndFlush(user);
		}
		else {
			throw new Exception("There is no User with username "+ username);
		}
		return "password updated";
	}

	@Override
	public String deleteUser(String username) {
		usersRepository.deleteById(username);
		return "User deleted";
	}

	@Override
	public List<Users> viewAllUsers() {
		return usersRepository.findAll();
	}

	@Override
	public String updateUserRole(String username, String role) throws Exception{
		Optional<Users> updateUser = usersRepository.findById(username);
		if(updateUser.isPresent()) {
			Users user = updateUser.get();
			user.setRole(role);
			usersRepository.saveAndFlush(user);
		}
		else {
			throw new Exception("There is no User with username "+ username);
		}
		return "role updated";
	}
	
}
