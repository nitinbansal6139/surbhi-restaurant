package com.online.restaurant.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.online.restaurant.entity.Users;
import com.online.restaurant.model.MyUserDetails;
import com.online.restaurant.repository.UsersRepository;

@Service
public class MyUserDetailsServiceImpl implements UserDetailsService{

    @Autowired
    UsersRepository usersRepository;
    
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Users> user = usersRepository.findByUserName(username);
        user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + username));
        return user.map(MyUserDetails::new).get();	
        }

}
