package com.online.restaurant.config;

import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.online.restaurant.entity.AuditLog;
import com.online.restaurant.repository.AuditRepository;
import com.online.restaurant.service.RestaurantItemService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@Aspect
public class AspectConfig {

//	@Autowired
//	RestaurantItemService restaurantItemService;
	
	@Autowired
	AuditRepository auditRepository;
	
	@Around("execution(public * com.online.restaurant.serviceImpl.*.*(..))")
	public Object logBeforeAndAfterAllMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
		auditRepository.saveAndFlush(AuditLog.builder().createDate(new Date())
				.discription(proceedingJoinPoint.getSignature().getName() + " Started").build());

		Object result = proceedingJoinPoint.proceed();
		auditRepository.saveAndFlush(AuditLog.builder().createDate(new Date())
			.discription(proceedingJoinPoint.getSignature().getName() + " Ended").build());
		return result;
	}
	
}
