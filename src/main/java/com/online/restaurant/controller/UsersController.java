package com.online.restaurant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.restaurant.entity.Users;
import com.online.restaurant.service.UsersService;

@RestController
@RequestMapping("/registerUser")
public class UsersController {

	@Autowired
	UsersService usersService;
	
	@PostMapping("/registrationByUser")
	public String registerByUser(String username, String password) {
		return usersService.registerUser(username, password);
	}
	
	@PostMapping("/registrationByAdmin")
	public String registerByAdmin(Users user) {
		return usersService.registerUserbyAdmin(user);
	}
	
	@GetMapping("/viewAllUsers")
	public List<Users> getAllUsers(){
		return usersService.viewAllUsers();
	}
	
	@DeleteMapping("/deleteUser")
	public String deleteUserByUsername(String userName) {
		return usersService.deleteUser(userName);
	}
	
	@PutMapping("/updateUserPassword")
	public String updateUserPassword(String username, String password) throws Exception {
		return usersService.updateUserPassword(username, password);
	}
	
	@PutMapping("/updateUserRole")
	public String updateUserRole(String username, String role) throws Exception {
		return usersService.updateUserRole(username, role);
	}
	
}
