package com.online.restaurant.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.online.restaurant.entity.FinancialReport;
import com.online.restaurant.entity.ItemDetails;
import com.online.restaurant.model.SelectItem;

@Service
public interface RestaurantItemService {

	List<ItemDetails> viewItemDetails();
	
	String selectItemDetailsbyId(SelectItem selectItem);
	
	String selectItemDetailsByIds(List<SelectItem> selectItem);
	
	List<FinancialReport> viewFinalBill();
	
	List<FinancialReport> viewDailyReport();
	
	List<FinancialReport> totalMonthlySale();
	
}
