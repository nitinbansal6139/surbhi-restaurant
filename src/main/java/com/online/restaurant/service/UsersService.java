package com.online.restaurant.service;

import java.util.List;

import com.online.restaurant.entity.Users;

public interface UsersService {
	
	String registerUser(String username, String password);
	
	String registerUserbyAdmin(Users user);
	
	String updateUserPassword(String username, String password) throws Exception;
	
	String updateUserRole (String username, String role) throws Exception;
	
	String deleteUser(String username);
	
	List<Users> viewAllUsers();

}
