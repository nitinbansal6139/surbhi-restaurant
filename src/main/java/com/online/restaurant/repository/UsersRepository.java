package com.online.restaurant.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.restaurant.entity.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, String>{
    Optional<Users> findByUserName(String userName);

}
